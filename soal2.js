function printpairs(arr, sum)
    {
        let s = new Set();
        for (let i = 0; i < arr.length; ++i)
        {
            let temp = sum - arr[i];

            if (s.has(temp)) {
                console.log(
                    sum + " is (" + arr[i]
                    + ", " + temp + ")");
            }
            s.add(arr[i]);
        }
    }

        let A = [ 1, 2, 3, 4 ];
        let n = 4;
        printpairs(A, n);
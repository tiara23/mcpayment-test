function subsetSum(numbers, target, partial) {
    let s, n, remaining;
  
    partial = partial || [];
  
    s = partial.reduce(function (a, b) {
      return a + b;
    }, 0);
  
    if (s === target) {
      console.log("%s=%s", partial.join("+"), target)
    }
  
    if (s >= target) {
      return;  
    }
  
    for (var i = 0; i < numbers.length; i++) {
      n = numbers[i];
      remaining = numbers.slice(i + 1);
      subsetSum(remaining, target, partial.concat([n]));
    }
  }
  
  subsetSum([3,1,4,2],4);